/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define PURPLE_PLUGINS 
#include <plugin.h>

#include <version.h>
#include <conversation.h>
#include <debug.h>
#include <gtkplugin.h>
#include <string.h>
#include "drawing.h"
#include "sketchboard.h"
#include "network.h"

static gboolean plugin_load(PurplePlugin *plugin) { 
	void *conv_handle = purple_conversations_get_handle();
	purple_debug_info(PLUGIN_ID, "SketchBoard plugin loaded.\n");
	purple_signal_connect(conv_handle, "receiving-im-msg", plugin, PURPLE_CALLBACK(receiving_im_msg_cb), NULL);
	purple_signal_connect(conv_handle, "writing-im-msg", plugin, PURPLE_CALLBACK(writing_im_msg_cb), NULL);
        purple_signal_connect(purple_blist_get_handle(), "blist-node-extended-menu", plugin, PURPLE_CALLBACK(context_menu_cb), plugin);

	session_initDrawingSessions();
	return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin) {
	void *conv_handle = purple_conversations_get_handle();
	purple_debug_info(PLUGIN_ID, "SketchBoard plugin unloaded.\n");
	purple_signal_disconnect(conv_handle, "receiving-im-msg", plugin, PURPLE_CALLBACK(receiving_im_msg_cb));
	purple_signal_disconnect(conv_handle, "writing-im-msg", plugin, PURPLE_CALLBACK(writing_im_msg_cb));
        purple_signal_disconnect(purple_blist_get_handle(), "blist-node-extended-menu", plugin, PURPLE_CALLBACK(context_menu_cb));

	session_killAllDrawingSessions();
	return TRUE;
}

static PurplePluginInfo info = {
	PURPLE_PLUGIN_MAGIC,
	PURPLE_MAJOR_VERSION,
	PURPLE_MINOR_VERSION,
	PURPLE_PLUGIN_STANDARD,                           /**< type           */
	PIDGIN_PLUGIN_TYPE,                               /**< ui_requirement */
	0,                                                /**< flags          */
	NULL,                                             /**< dependencies   */
	PURPLE_PRIORITY_DEFAULT,                          /**< priority       */
	PLUGIN_ID,		                          /**< id             */
	DISPLAY_NAME,                                     /**< name           */
	DISPLAY_VERSION,                                  /**< version        */
	DISPLAY_SUMMARY,				  /**  summary        */
	DISPLAY_DESCRIPTION,                              /**  description    */
	DISPLAY_AUTHOR,					  /**< author         */
	DISPLAY_HOMEPAGE,                                 /**< homepage       */
	plugin_load,                                      /**< load           */
	plugin_unload,                                    /**< unload         */
	NULL,                                             /**< destroy        */
	NULL,                                             /**< ui_info        */
	NULL,                                             /**< extra_info     */
	NULL,
	NULL,
	/* Padding */
	NULL,
	NULL,
	NULL,
	NULL
};

static void init_plugin(PurplePlugin *plugin) { 
}

PURPLE_INIT_PLUGIN(sketchboard, init_plugin, info)
