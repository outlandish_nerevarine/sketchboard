/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SB__PLUGIN__
#define __SB__PLUGIN__

#define PLUGIN_ID 	 	"sketchboard-plugin"
#define DISPLAY_NAME		"SketchBoard"
#define DISPLAY_VERSION 	"0.1"
#define DISPLAY_SUMMARY		"SketchBoard Plugin"
#define DISPLAY_DESCRIPTION 	"Draw collaboratively with your friends."  
#define DISPLAY_AUTHOR		"Daniel Koestler"
#define	DISPLAY_HOMEPAGE	"http://" // TODO: homepage



#endif
