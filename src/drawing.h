/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SB_DRAWING__
#define  __SB_DRAWING__

#include <glib.h>
#include <blist.h>
#include <gtkplugin.h>
#include <gtk/gtk.h>
#include <stdbool.h>

#define MAX_DRAWING_POINTS_PER_MESSAGE 65536 // can be quite large. Just a sanity check. Don't make it larger than MAX_INT, however, nor smaller than the number of GdkPoints you could hold in a MESSAGE_MAX_LENGTH
#define	WINDOW_WIDTH 550
#define WINDOW_HEIGHT 550
#define DRAWING_AREA_WIDTH 525
#define DRAWING_AREA_HEIGHT 445

enum brush { PENCIL, LINE, RECTANGLE, CIRCLE, ERASER, TEXT, CLEAN };
GHashTable *drawingSessions;

typedef struct DrawingWindow DrawingWindow;
typedef struct DrawingSession DrawingSession;

struct DrawingWindow { 
	GtkWidget	*window;
	GtkWidget	*fixed; // Fixed container
	GtkWidget	*drawing_area; 
	GtkWidget	*label_brush;
	GtkWidget	*label_size;
	GtkWidget	*label_color;
	GdkPixmap	*pixmap;
	gdouble		brush_size;
	GdkGC		*color_gc;
	GdkGC		*friend_color_gc;
	GdkColor	color;
	GdkColor	friend_color;
	enum brush	currentBrush;
	GArray		*drawingPoints;
	DrawingSession	*owner;
	GArray		*incomingDrawingPoints;
	int		numIncomingPoints;
};

struct DrawingSession {
	PurpleBuddy 		*buddy;
	PurpleConversation 	*conv;
	gboolean		isWindowOpen;
	DrawingWindow		*drawingWindow;
};

void session_initDrawingSessions(); 
void session_killAllDrawingSessions();
void session_initNewSession(DrawingSession *session);
void session_initNewDrawingWindow(DrawingWindow *drawingWindow);
void session_track(const char *buddyName, DrawingSession *session); 
void session_untrack(const char *buddyName); 
DrawingSession* session_get_session(const char *buddyName);
bool drawing_create_drawing_window(DrawingSession *session);
bool session_handle_incoming_drawpoints(DrawingSession *session, const char *message);
void session_cancel_incoming_drawpoints(DrawingSession *session);
void pencil_clicked(GtkWidget *widget, gpointer _drawingSession); 
void eraser_clicked(GtkWidget *widget, gpointer _drawingSession); 
void context_menu_cb(PurpleBlistNode *node, GList **menu, gpointer *data); 
void draw_with_cb(PurpleBlistNode *node, gpointer *data); 


#endif
