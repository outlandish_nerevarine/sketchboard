/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing.h"
#include <stdbool.h>
#include "sketchboard.h"
#include <debug.h>
#include "network.h"
#include <string.h>

#define DEFAULT_BRUSH_SIZE 8

GHashTable *drawingSessions = 0;

static void _destroy_drawing_session(DrawingSession *drawingSession);
static void draw_to_pixmap(GtkWidget *widget, DrawingWindow *drawingWindow, GArray *drawingPoints);
static gint configure_event(GtkWidget *widget, GdkEventConfigure *event, gpointer _drawingWindow); 
static gint expose_event(GtkWidget *widget, GdkEventExpose *event, gpointer drawingWindow); 
static gint button_press_event(GtkWidget *widget, GdkEventButton *event, gpointer _drawingWindow); 
static gint button_release_event(GtkWidget *widget, GdkEventButton *event, gpointer _drawingWindow); 
static gboolean motion_notify_event(GtkWidget *widget, GdkEventMotion *event, gpointer _drawingWindow); 
static void window_destroy_event(GtkObject *object, gpointer _drawingWindow); 


//TODO: DEBUG: test to make sure it's freeing the drawingWindow memory properly!
static void _destroy_drawing_session(DrawingSession *drawingSession) { 
	if (drawingSession->drawingWindow && drawingSession->drawingWindow->drawingPoints) { 
		g_array_free(drawingSession->drawingWindow->drawingPoints, TRUE);
	}
	g_free(drawingSession->drawingWindow);
	g_free(drawingSession);
}

/**
 * Initializes the internal hash table
 * Must be called before using the other session_* functions!
 * Call session_killAllDrawingSessions() when you're done
 */
void session_initDrawingSessions() { 
	if (drawingSessions) return;
	drawingSessions = g_hash_table_new_full((GHashFunc)g_str_hash, 
						(GEqualFunc)g_str_equal,
						(GDestroyNotify)g_free,
						(GDestroyNotify)_destroy_drawing_session);
}


/**
 * Stops tracking all sessions. 
 * Call this before the plugin unloads. This will automatically
 * free the memory used by the DrawingSession structs 
 */
void session_killAllDrawingSessions() { 
	g_hash_table_destroy(drawingSessions);
}

void session_initNewSession(DrawingSession *session) { 
	session->buddy = NULL;
	session->conv = NULL;
	session->isWindowOpen = FALSE;
}

void session_initNewDrawingWindow(DrawingWindow *drawingWindow) { 
	drawingWindow->window	 	= NULL;
	drawingWindow->fixed		= NULL;
	drawingWindow->drawing_area 	= NULL;
	drawingWindow->pixmap 		= NULL;
	drawingWindow->label_brush 	= NULL;
	drawingWindow->label_size 	= NULL;
	drawingWindow->label_color	= NULL;
	drawingWindow->color_gc		= NULL;
	drawingWindow->friend_color_gc	= NULL;
	drawingWindow->brush_size	= DEFAULT_BRUSH_SIZE;
	drawingWindow->currentBrush 	= PENCIL;
	drawingWindow->drawingPoints	= NULL;
	drawingWindow->incomingDrawingPoints	= NULL;
	drawingWindow->numIncomingPoints = 0;
	drawingWindow->owner		= NULL;
}

void session_track(const char *buddyName, DrawingSession *session) { 
	char *key = g_strdup(buddyName); // key deleted by GDestroyNotify function for key
	purple_debug_misc(PLUGIN_ID, "Tracking a buddy: %s\n", buddyName);
	g_hash_table_insert(drawingSessions, key, session);
}

/**
 * Stops tracking a session. Frees the memory for the DrawingSession and DrawingWindow
 * @param buddyName The name of the buddy, which is used as a key in the internal hash table
 */
void session_untrack(const char *buddyName) { 
	purple_debug_misc(PLUGIN_ID, "Untracking a buddy: %s\n", buddyName);
	g_hash_table_remove(drawingSessions, buddyName);
}

/**
 * Returns a pointer to the requested DrawingSession
 * @param buddyName The name of the buddy, which is used a key in the internal hash table
 * @returns NULL if the session isn't being tracked
 */
DrawingSession* session_get_session(const char *buddyName) { 
	return g_hash_table_lookup(drawingSessions, buddyName);	
}


static void draw_to_pixmap(GtkWidget *widget, DrawingWindow *drawingWindow, GArray *drawingPoints) { 
	if (!drawingWindow->pixmap) return;

	switch(drawingWindow->currentBrush) { 
		case PENCIL: 
			//TODO: Don't call this every time.
			gdk_gc_set_line_attributes(widget->style->black_gc, 15, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
			gdk_draw_lines(drawingWindow->pixmap, widget->style->black_gc, (GdkPoint*)drawingPoints->data, drawingPoints->len);
			break;
		case ERASER:
			//TODO: Don't call this every time.
			gdk_gc_set_line_attributes(widget->style->white_gc, 30, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
			gdk_draw_lines(drawingWindow->pixmap, widget->style->white_gc, (GdkPoint*)drawingPoints->data, drawingPoints->len);
			break;
		default:
			break;
	}
	gtk_widget_queue_draw(widget);

}

static gint configure_event(GtkWidget *widget, GdkEventConfigure *event, gpointer _drawingWindow) { 
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	GdkPixmap *pixmap = drawingWindow->pixmap;
	if (pixmap) { 
		gdk_pixmap_unref(pixmap);
	}

	pixmap = gdk_pixmap_new(widget->window, widget->allocation.width, widget->allocation.height, -1);
	drawingWindow->pixmap = pixmap;
	gdk_draw_rectangle(pixmap, widget->style->white_gc, TRUE, 0, 0, widget->allocation.width, widget->allocation.height);

	return TRUE;
}

static gint expose_event(GtkWidget *widget, GdkEventExpose *event, gpointer drawingWindow) { 
	gdk_draw_pixmap(widget->window, widget->style->fg_gc[GTK_WIDGET_STATE(widget)], ((DrawingWindow*)drawingWindow)->pixmap, event->area.x, event->area.y, event->area.x, event->area.y, event->area.width, event->area.height);

	return FALSE;
}

static gint button_press_event(GtkWidget *widget, GdkEventButton *event, gpointer _drawingWindow) { 
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	// Clear the point array
	g_array_set_size(drawingWindow->drawingPoints, 0);	

	GdkPoint point = { event->x, event->y };
	g_array_append_val(drawingWindow->drawingPoints, point);

	draw_to_pixmap(widget, drawingWindow, drawingWindow->drawingPoints);
	return TRUE;
}

static gint button_release_event(GtkWidget *widget, GdkEventButton *event, gpointer _drawingWindow) { 
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	if (event->type == GDK_BUTTON_RELEASE) { 
		GdkPoint point = { event->x, event->y };
		g_array_append_val(drawingWindow->drawingPoints, point);

		network_queue_outbound_drawpoints(drawingWindow->owner,
						  drawingWindow->currentBrush,
					 	  drawingWindow->drawingPoints, 0);

		draw_to_pixmap(widget, drawingWindow, drawingWindow->drawingPoints);
		gtk_widget_queue_draw(widget);
		g_array_set_size(drawingWindow->drawingPoints, 0);	
	}

	return TRUE;
}

static gboolean motion_notify_event(GtkWidget *widget, GdkEventMotion *event, gpointer _drawingWindow) { 
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	int x, y;
	GdkModifierType state; 

	if (event->is_hint) { 
		gdk_window_get_pointer(event->window, &x, &y, &state);
	} else { 
		x = event->x;
		y = event->y;
		state = event->state;
	}
	
	if (state & GDK_BUTTON1_MASK && drawingWindow->pixmap) { 
		GdkPoint point = { x, y };
		g_array_append_val(drawingWindow->drawingPoints, point);
		draw_to_pixmap(widget, drawingWindow, drawingWindow->drawingPoints);
	}

	return TRUE;
}

static void window_destroy_event(GtkObject *object, gpointer _drawingWindow) { 
	if (!_drawingWindow) return;
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	if (!drawingWindow->owner) return;
	DrawingSession *session = drawingWindow->owner;
	
	session_untrack(session->buddy->name);
	network_notify_buddy_session_terminated(session);
}

/**
 * Creates a new drawing window for a given session and displays it. 
 * @param An initialized session that also contains an initialized drawingWindow.
 * 	Use the session_init* functions to initialize these objects.
 * @returns FALSE if something failed. TRUE otherwise.
 */
bool drawing_create_drawing_window(DrawingSession *session) { 
	if (!session || !session->drawingWindow || !session->buddy || !session->buddy->name) return FALSE; 
	GtkWidget	*window; 
	GtkWidget	*drawing_area;
	GtkWidget	*fixed; 
	GtkWidget	*vbox;

	GtkWidget	*toolbar;
	GtkToolItem	*pencil;
	GtkToolItem	*eraser;

	session->drawingWindow->drawingPoints = g_array_new(FALSE, FALSE, sizeof(GdkPoint));
	session->drawingWindow->incomingDrawingPoints = g_array_new(FALSE, FALSE, sizeof(GdkPoint));

	// Main window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	session->drawingWindow->window = window;
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), WINDOW_WIDTH, WINDOW_HEIGHT);
	char *title = g_strdup_printf("%s: %s", DISPLAY_NAME, session->buddy->name);
	gtk_window_set_title(GTK_WINDOW(window), title);
	g_free(title);

	// Fixed layout container
	fixed = gtk_fixed_new();
	session->drawingWindow->fixed = fixed;
	//gtk_container_add(GTK_CONTAINER(window), fixed);

	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	// Toolbar
	toolbar = gtk_toolbar_new();
	gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
	gtk_container_set_border_width(GTK_CONTAINER(toolbar), 2);

	pencil = gtk_tool_button_new(gtk_image_new_from_file("pixmaps/pencil.png"), NULL);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), pencil, -1);

	eraser = gtk_tool_button_new(gtk_image_new_from_file("pixmaps/eraser.png"), NULL);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), eraser, -1);

	gtk_box_pack_start(GTK_BOX(vbox), toolbar, FALSE, FALSE, 5);

	// Drawing area
	drawing_area = gtk_drawing_area_new();
	session->drawingWindow->drawing_area = drawing_area;
	gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_area), DRAWING_AREA_WIDTH, DRAWING_AREA_HEIGHT);
	//gtk_fixed_put(GTK_FIXED(fixed), drawing_area, 10, 75);
	// TODO: remove fixed
	gtk_box_pack_start(GTK_BOX(vbox), drawing_area, FALSE, FALSE, 5);

	// Events
	g_signal_connect(GTK_OBJECT(drawing_area), "expose_event", (GtkSignalFunc)expose_event, (gpointer)session->drawingWindow);
	g_signal_connect(GTK_OBJECT(drawing_area), "configure_event", (GtkSignalFunc)configure_event, (gpointer)session->drawingWindow);
	g_signal_connect(GTK_OBJECT(drawing_area), "motion_notify_event", (GtkSignalFunc)motion_notify_event, (gpointer)session->drawingWindow);
        g_signal_connect(GTK_OBJECT(drawing_area), "button_press_event", (GtkSignalFunc)button_press_event, (gpointer)session->drawingWindow);
        g_signal_connect(GTK_OBJECT(drawing_area), "button_release_event", (GtkSignalFunc)button_release_event, (gpointer)session->drawingWindow);
	g_signal_connect(GTK_OBJECT(pencil), "clicked", G_CALLBACK(pencil_clicked), session->drawingWindow);
	g_signal_connect(GTK_OBJECT(eraser), "clicked", G_CALLBACK(eraser_clicked), session->drawingWindow);
        
	g_signal_connect(GTK_OBJECT(window), "destroy", (GtkSignalFunc)window_destroy_event, (gpointer)session->drawingWindow);

	gtk_widget_set_events(drawing_area, GDK_EXPOSURE_MASK | GDK_LEAVE_NOTIFY_MASK | GDK_BUTTON_PRESS_MASK | GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_HINT_MASK);
	purple_debug_misc(PLUGIN_ID, "Displaying drawing window for %s.\n", session->buddy->name);
	session->isWindowOpen = true;
	gtk_widget_show_all(window);
	
	return TRUE;
}

/**
 * This function handles a SketchBoard protocol message that communicates either an array of drawpoints or a continuation 
 *  of a previously sent array of drawpoints. It will parse the message and draw the points once all the data has been received.
 *  The first message received should be a MESSAGE_ARRAY_DRAW and define n GdkPoints. This message should also supply
 *  up to n GdkPoints. If it supplies less than n GdkPoints, this function must be called in the future with continuation message(s).
 * Once n GdkPoints have been parsed this function will display them in the drawingWindow. This process can also be cancelled.
 *  See the definitions of MESSAGE_ARRAY_DRAW_PREFIX and MESSAGE_ARRAY_CONTINUATION_PREFIX to see the expected input format.
 *
 * Calling this function with a new drawing request when it's expecting to finish receiving drawpoints will cancel the current drawing task and result in an error. Calling it with continuation data when it's expecting a new drawing command will result in an error.
 *
 * @param session An initialized and valid DrawingSession with an initialized and valid drawingWindow
 * @param message A null-terminated message that defines either an array draw message, or a continuation of an array draw message.
 * @returns FALSE if something went wrong. TRUE if the message was successfully parsed
 **/
bool session_handle_incoming_drawpoints(DrawingSession *session, const char *message) { 
	gchar *buffer;
	char *protocol, *command, *tool, *num, *x, *y;
	int n, j, xCoord, yCoord; // n = num GdkPoints.
	GArray *points	= session->drawingWindow->incomingDrawingPoints;
	buffer = g_strdup(message);

	if (session->drawingWindow->incomingDrawingPoints->len == 0) { 
		if ( strncmp(message, MESSAGE_ARRAY_DRAW_PREFIX, strlen(MESSAGE_ARRAY_DRAW_PREFIX)) != 0 ) { 
			purple_debug_misc(PLUGIN_ID, "We're expecting a new drawing command, but we received something else! (Continuation data for a command that was never started?) We'll cancel the old drawing command and start again.\n");
			g_array_set_size(points, 0);
			return FALSE;

		}
		// Handle a new drawing command
		protocol = strtok(buffer, ":");
		command  = strtok(NULL, ":");
		tool	 = strtok(NULL, ":");
		num	 = strtok(NULL, ":");
		n = (int)strtol(num, NULL, 10);
		if (n <= 0 || n > MAX_DRAWING_POINTS_PER_MESSAGE) return FALSE;

		purple_debug_misc(PLUGIN_ID, "Drawing started: %s:%s:%s:%s\n", protocol, command, tool, num);
		for (j=0; j<n; j++) { 
			x = strtok(NULL, ":");
			y = strtok(NULL, ":");
			if ( !x || !y ) break; // continuation data is required!
			xCoord = (int)strtol(x, NULL, 10);
			yCoord = (int)strtol(y, NULL, 10);
			purple_debug_misc(PLUGIN_ID, "Received a GdkPoint: %i/%i: {%i, %i}.\n", j+1, n, xCoord, yCoord);
			// TODO: Should points that exceed the drawing area width and height (DRAWING_AREA_WIDTH, HEIGHT) be modified? Any harm in drawing outside the boundaries? (Check security of gdk_draw_lines)
			GdkPoint point = { xCoord, yCoord };
			g_array_append_val(points, point);
		}
		// Parsing complete! We can either draw or return successfully and await continuation data
		if (j == n) { 
			// All points received!
			draw_to_pixmap(session->drawingWindow->window, session->drawingWindow, points);
			g_array_set_size(points, 0);
		} else { 
			// We require continuation data. Let's record the total number of points we'll need
			session->drawingWindow->numIncomingPoints = n;
		}

	} else { 
		if ( strncmp(message, MESSAGE_ARRAY_CONTINUATION_PREFIX, strlen(MESSAGE_ARRAY_CONTINUATION_PREFIX)) != 0 ) { 
			purple_debug_misc(PLUGIN_ID, "We're expecting continuation data to finish an old drawing command, but we received something else, such as a new drawing request! (The current drawing command has been cancelled.)\n");
			session_cancel_incoming_drawpoints(session);
			return FALSE;
		}
		// Receive more GdkPoints for an existing drawing command
		protocol = strtok(buffer, ":");
		command  = strtok(NULL, ":");
		purple_debug_misc(PLUGIN_ID, "Drawing continued: %s:%s:\n", protocol, command);
		for (j=points->len; j<session->drawingWindow->numIncomingPoints; j++) {
			x = strtok(NULL, ":");
			y = strtok(NULL, ":");
			if ( !x || !y ) break; // continuation data is required!
			xCoord = (int)strtol(x, NULL, 10);
			yCoord = (int)strtol(y, NULL, 10);
			purple_debug_misc(PLUGIN_ID, "Received a GdkPoint: %i/%i: {%i, %i}.\n", j+1, session->drawingWindow->numIncomingPoints, xCoord, yCoord);
			GdkPoint point = { xCoord, yCoord };
			g_array_append_val(points, point);
		}
		if (j == session->drawingWindow->numIncomingPoints) { 
			// All points received! 
			draw_to_pixmap(session->drawingWindow->window, session->drawingWindow, points);
			g_array_set_size(points,0);
		}
	}

	g_free(buffer);
	return TRUE;
}

/**
 * If SketchBoard has received a drawing message of n GdkPoints in a previous message and was only supplied with n-1 or fewer,
 *  it will be waiting for a continuation message. Call this function to cancel the attempted drawing and revert back to a state
 *  where you can receive a brand new drawing request.
 *
 * @param session An initialized and valid DrawingSession with an initialized and valid drawingWindow
 **/
void session_cancel_incoming_drawpoints(DrawingSession *session) { 
	g_array_set_size(session->drawingWindow->incomingDrawingPoints, 0);
}

void pencil_clicked(GtkWidget *widget, gpointer _drawingWindow) { 
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	drawingWindow->currentBrush = PENCIL;
}

void eraser_clicked(GtkWidget *widget, gpointer _drawingWindow) { 
	DrawingWindow *drawingWindow = (DrawingWindow*)_drawingWindow;
	drawingWindow->currentBrush = ERASER;
}

void context_menu_cb(PurpleBlistNode *node, GList **menu, gpointer *data) { 
        PurpleMenuAction *action;
	PurpleBuddy *buddy;
	PurpleAccount *account;

        if (!PURPLE_BLIST_NODE_IS_BUDDY(node) && !PURPLE_BLIST_NODE_IS_CONTACT(node) &&
            !(purple_blist_node_get_flags(node) & PURPLE_BLIST_NODE_FLAG_NO_SAVE)) { 
        	return;
	}
	buddy = (PurpleBuddy *)node;
        account = buddy->account;
	if (account == NULL) return;

	// TODO: rerplace name with buddy->name
        action = purple_menu_action_new("Start drawing", PURPLE_CALLBACK(draw_with_cb), buddy, NULL);
        (*menu) = g_list_prepend(*menu, action);
        purple_debug_info(PLUGIN_ID, "Context menu created, but not drawing yet.\n");
}

void draw_with_cb(PurpleBlistNode *node, gpointer *data) { 
	PurpleBuddy *buddy = (PurpleBuddy*)data;
	purple_debug_info(PLUGIN_ID, "Let's draw with %s.\n", buddy->name);
}

