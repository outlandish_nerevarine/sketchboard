/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <debug.h>
#include <string.h>
#include <plugin.h>
#include <gtkplugin.h>
#include <conversation.h>
#include "drawing.h"
#include "sketchboard.h"
#include "network.h"

/**
 * This function takes a GArray* of GdkPoints, an index to start at, and sends out drawing commands for them.
 * @param *session The DrawingSession that generated these drawing points
 * @param currentBrush An enum brush type indicating what tool was used to create the points
 * @param *datapoints A GArray of an arbitrary size that contains valid GdkPoints
 * @param index This indicates at what position in the array you want to start sending points. You'll probably want to set this to 0 so it sends all the drawing data you generated.
 */
void network_queue_outbound_drawpoints(DrawingSession *session, enum brush currentBrush, GArray *datapoints, int index) { 
	// GdkPoints store gints. God help us all if you have greater than 2^31*2^31 pixels
	int x, availableMessageLen;
	char strDigits[24];
	char *compositionString = g_malloc(MESSAGE_MAX_LENGTH);

	// If index is 0, this is a MESSAGE_ARRAY_DRAW_PREFIX
	// If index is > 0 it's MESSAGE_ARRAY_CONTINUATION_PREFIX, as it's a broken-up message
	if (datapoints->len <= 0 || index >= datapoints->len || !session || !session->conv) return; 
	if (index == 0) { 
		switch (currentBrush) { 
			case PENCIL: 
				sprintf(compositionString,"%s%s%i%s",MESSAGE_ARRAY_DRAW_PREFIX,"P:",datapoints->len, ":");
				break;
			case ERASER:
				sprintf(compositionString,"%s%s%i%s",MESSAGE_ARRAY_DRAW_PREFIX,"E:",datapoints->len, ":");
				break;

			default:
				return;
		}
	} else { 
		sprintf(compositionString,"%s",MESSAGE_ARRAY_CONTINUATION_PREFIX);
	}

	availableMessageLen = MESSAGE_MAX_LENGTH - strlen(compositionString);
	for (x=index; x<datapoints->len;  x++) { 
		if (strlen(compositionString) > availableMessageLen) {
			break;
		}
		snprintf(strDigits,24,"%i%s%i%s",g_array_index(datapoints,GdkPoint,x).x,":",g_array_index(datapoints,GdkPoint,x).y,":");
		strcat(compositionString, strDigits);
	}

	// Send the IM, then repeat this process so we can send continuation arrays, if necessary
	purple_conv_im_send_with_flags(PURPLE_CONV_IM(session->conv), compositionString, PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_INVISIBLE);
	g_free(compositionString);
	if (x < datapoints->len) { 
		network_queue_outbound_drawpoints(session, currentBrush, datapoints, x);
	}	
}

/**************************************************************************
 * Called when we're in the process of receiving an IM
 * If message doesn't match the prefix, ignore it. 
 * Assumes **buffer is null terminated (as all other plugins seem to do)
 *
 * @returns TRUE if message is handled (SketchBoard-related). FALSE otherwise
 **************************************************************************/
gboolean receiving_im_msg_cb(PurpleAccount *account, char **sender, char **buffer, PurpleConversation *conv, PurpleMessageFlags *flags, void *data) { 
	DrawingSession  *newSession;
	DrawingWindow	*drawingWindow;
	PurpleBuddy	*buddy;
	const char	*name;
	name = purple_conversation_get_name(conv);

	if (!sender || !*sender || !buffer || !*buffer || !name || !conv) 
		return FALSE;

	// Is this a SketchBoard-related message?
	if ( strncmp(*buffer, PROTOCOL_PREFIX, strlen(PROTOCOL_PREFIX)) != 0 ) { 
		return FALSE;
	}
	*flags |= PURPLE_MESSAGE_INVISIBLE | PURPLE_MESSAGE_NO_LOG;


	purple_debug_misc(PLUGIN_ID, "receiving-im-msg (%s, %s, %s, %s, %d)\n", purple_normalize(account, purple_account_get_username(account)), *sender, *buffer, name, *flags);

	if (!account || (buddy = purple_find_buddy(account, name)) == NULL) {
		purple_debug_warning(PLUGIN_ID, "Tried to process a drawing message for someone not on your buddy list! (Or I wasn't able to find that buddy based on the provided account information.");
		return TRUE; // technically we handled the message, we're just not happy about it
	}

	// what message did we get? session: start/end, draw, or batched draw messages?
	if ( strncmp(*buffer, MESSAGE_START_SESSION, strlen(MESSAGE_START_SESSION)) == 0 ) { 
		//session start
		purple_debug_misc(PLUGIN_ID, "Tracking a new buddy: %s.\n", name);

		newSession 	= g_new(DrawingSession, 1);
		drawingWindow 	= g_new(DrawingWindow, 1);
		session_initNewSession(newSession);
		session_initNewDrawingWindow(drawingWindow);

		drawingWindow->owner = newSession;
		newSession->conv = conv;
		newSession->buddy = buddy;
		newSession->drawingWindow = drawingWindow;
		session_track(purple_conversation_get_name(conv), newSession);
		drawing_create_drawing_window(newSession);
		return TRUE;
	} 

	newSession = session_get_session(name);
	if (!newSession) { 
		purple_debug_warning(PLUGIN_ID, "Received a message for %s, but we don't have an active session with him/her!", name);
		return FALSE;
	}
	// From this point on we know we have an active session and must handle this SketchBoard-related message (assuming it matches a valid command)

	if ( strncmp(*buffer, MESSAGE_END_SESSION, strlen(MESSAGE_END_SESSION)) == 0 ) { 
		// session end
		purple_debug_misc(PLUGIN_ID, "Untracking a buddy: %s.\n", name);
		session_untrack(purple_conversation_get_name(conv));	
	} else if ( (strncmp(*buffer, MESSAGE_ARRAY_DRAW_PREFIX, strlen(MESSAGE_ARRAY_DRAW_PREFIX)) == 0) || 
		    (strncmp(*buffer, MESSAGE_ARRAY_CONTINUATION_PREFIX, strlen(MESSAGE_ARRAY_CONTINUATION_PREFIX)) == 0) ) {  
		purple_debug_misc(PLUGIN_ID, "Received a message for either array drawing data or cotinuation data for %s.\n", name);
		session_handle_incoming_drawpoints(newSession, *buffer);
	}
	return TRUE;
}

// TODO: Use this to listen for error messages on IM sending
gboolean writing_im_msg_cb(PurpleAccount *account, const char *who, char **buffer, PurpleConversation *conv, PurpleMessageFlags flags, void *data) {
	purple_debug_misc(PLUGIN_ID, "writing-im-msg (%s, %s, %s)\n", purple_normalize(account, purple_account_get_username(account)), purple_conversation_get_name(conv), *buffer);

	return FALSE;
}

/**
 * Takes a valid session and notifies the buddy that you've ended the drawing session.
 * @param session A pointer to a valid, initialized DrawingSession
 **/
void network_notify_buddy_session_terminated(DrawingSession *session) { 
	if (!session || !session->buddy || !session->buddy->name || !session->conv) return;
	purple_debug_misc(PLUGIN_ID, "Sending an IM to end the session with %s\n", session->buddy->name);
	purple_conv_im_send_with_flags(PURPLE_CONV_IM(session->conv), MESSAGE_END_SESSION, PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_INVISIBLE);
		
}
