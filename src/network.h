/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SB_NETWORK__
#define __SB_NETWORK__

#define USE_BATCHED_MODE	FALSE
#define PROTOCOL_PREFIX		":?SBv1" // this must be a common root to the messages
#define MESSAGE_PREFIX		PROTOCOL_PREFIX ":"  //Not batch or array, just a single command 
#define MESSAGE_START_SESSION	MESSAGE_PREFIX "START"
#define MESSAGE_END_SESSION	MESSAGE_PREFIX "END"
#define MESSAGE_DRAW		MESSAGE_PREFIX "DRAW"
#define MESSAGE_PENCIL_PREFIX	"P"
#define MESSAGE_ARRAY_DRAW_PREFIX		PROTOCOL_PREFIX "A:D:" 
// :?SBv1A:D:<tool>:<n GdkPoints in total>:x1:y1:x2:y2:...:xn:yn:
#define MESSAGE_ARRAY_CONTINUATION_PREFIX	PROTOCOL_PREFIX "A:C:"
// :?SBv1A:D:<tool>:<n GdkPoints in total>:x1:y1:x2:y2:...:xa:ya:
// :?SBv1A:C:xa+1:ya+1:xa+2:ya+2:xa+3:ya+3:...:xn:yn
// There can be multiple continuations
#define MESSAGE_MAX_LENGTH      1024   // The maximum number of characters that can be sent



#define MESSAGE_BATCH_PREFIX    PROTOCOL_PREFIX "B:" // If you must change these prefixes, make this one bigger than just PREFIX
#define MESSAGE_BATCH_TIMEOUT_S    2   // If no drawing commands have been issued in this number of seconds, then send the batch without waiting any longer
#define MESSAGE_MAX_TO_BATCH	  10   // The maximum number of messages that can be combined
#define MESSAGE_PROTOTYPE_LENGTH  27   // Length of the drawing message in characters, minus text length
// MESSAGE PROTOTYPE: :PREFIX:TOOL:COLOR:x1:y1:x2:y2:text:filled(boolean):
// Size (bytes)             5    1     2  2  2  2  2    ?      1  + 1 character per delimeter
// Batch format:   :?SBv1B:n:<message 1><message 2>...<message n>
// Where n is the number of messages in the batch

gboolean receiving_im_msg_cb(PurpleAccount *account, char **sender, char **buffer, PurpleConversation *conv, PurpleMessageFlags *flags, void *data); 
gboolean writing_im_msg_cb(PurpleAccount *account, const char *who, char **buffer, PurpleConversation *conv, PurpleMessageFlags flags, void *data);
void network_queue_outbound_drawpoints(DrawingSession *session, enum brush currentBrush, GArray *datapoints, int index);
void network_notify_buddy_session_terminated(DrawingSession *session);
void network_notify_buddy_session_begin_request(PurpleBuddy *buddy);

#endif
