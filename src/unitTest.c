/*
 * SketchBoard - Collaborate and Draw
 * Copyright (C) 2013 Daniel Koestler
 * Based on Pidgin PaintBoard (https://code.google.com/p/pidgin-paintboard/)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "drawing.h"
#include <blist.h>
#include "debug.h"

void display_tests() { 
	DrawingSession *newSession = g_new(DrawingSession, 1);
	session_initNewSession(newSession);;

	DrawingWindow *drawingWindow = g_new(DrawingWindow, 1);
	session_initNewDrawingWindow(drawingWindow);
	drawingWindow->owner = newSession;

	PurpleBuddy *buddy = g_new(PurpleBuddy, 1);
	buddy->name = "user1@domain.com";

	PurpleConversation *conv = g_new(PurpleConversation, 1);

	newSession->drawingWindow = drawingWindow;
	newSession->buddy = buddy;
	newSession->conv = conv;

	drawing_create_drawing_window(newSession);

	gtk_main();

	g_free(buddy);
	g_free(drawingWindow);
	g_free(newSession);
	g_free(conv);
}


int run_tests() { 
	purple_debug_init();
	purple_debug_set_enabled(true);
	session_initDrawingSessions(); 

	DrawingSession *newSession = g_new(DrawingSession, 1);
	g_assert(newSession != NULL);
	printf("newSession: %p", newSession);

	session_initNewSession(newSession);
	g_assert(newSession->buddy == NULL);

	session_track("user1@domain.com", newSession);	
	g_assert(session_get_session("user1@domain.com") == newSession);

	DrawingSession *someSession = session_get_session("user1@domain.com");
	printf("someSession: %p", someSession);

	session_untrack("user1@domain.com");
	g_assert(session_get_session("user1@domain.com") == NULL); 

	session_killAllDrawingSessions(); 
	
	display_tests();

	return 0;
}

int main(int argc, char *argv[]) { 
	gtk_init(&argc, &argv);
	return run_tests();
}
